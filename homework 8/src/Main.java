import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Выберите животное из списка: собак, кошка, пантера, медведь ");
        String pet = scanner.nextLine();

        switch(pet){
            case "собака":
                System.out.println("Вы выбрали домашнее животное");
                break;
            case "кошка":
                System.out.println("Вы взялии домашнее животное");
                break;
            case "пантера":
                System.out.println("Вы выбрали дикое животное");
                break;
            case "медведь":
                System.out.println("Вы взяли дикое животное");
                break;
            default:
                System.out.println("Вы выбрали не то животное ");

        }
    }
}